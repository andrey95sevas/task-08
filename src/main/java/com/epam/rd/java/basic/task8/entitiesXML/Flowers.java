package com.epam.rd.java.basic.task8.entitiesXML;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "flower"
})
@XmlRootElement(name = "flowers")
public class Flowers {

    @XmlElement(name = "Flower", required = true)
    private List<Flower> flowerList = new ArrayList<>();


    public List<Flower> getFlowerList() {
        if (flowerList == null) {
            flowerList = new ArrayList<>();
        }
        return flowerList;

    }
}
