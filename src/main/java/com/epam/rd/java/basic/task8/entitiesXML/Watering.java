package com.epam.rd.java.basic.task8.entitiesXML;


import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "value"
})
public class Watering {
    @XmlValue
    @XmlSchemaType(name = "positiveInteger")
    public int value;
    @XmlAttribute(name = "measure", required = true)
    public String measure;

    public Watering() {
    }

    public Watering(int value, String measure) {
        this.value = value;
        this.measure = measure;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setMeasure(String value) {
        measure = value;
    }

    public int getValue() {
        return value;
    }

    public String getMeasure() {
        return measure;
    }

    @Override
    public String toString() {
        return "Watering{" +
                "value=" + value +
                ", measure='" + measure + '\'' +
                '}';
    }
}

