package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entitiesXML.Flowers;
import com.epam.rd.java.basic.task8.handler.Constants;
import com.epam.rd.java.basic.task8.sorter.Sorter;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.parse(xmlFileName);
		Flowers flowers = domController.getFlowers();
		Sorter.sortByOrigin(flowers);
		String outputXmlFile = "output.dom.xml";
		DOMController.saveToXML(flowers, outputXmlFile);
		System.out.println(Constants.OUTPUT + outputXmlFile);

		// PLACE YOUR CODE HERE

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parse(true);
		flowers = saxController.getFlowers();
		Sorter.sortByName(flowers);
		outputXmlFile = "output.sax.xml";
		DOMController.saveToXML(flowers, outputXmlFile);
		System.out.println("output == > " + outputXmlFile);

		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();
		flowers = staxController.getFlowers();
		Sorter.sortBySoil(flowers);
		outputXmlFile = "output.stax.xml";
		DOMController.saveToXML(flowers, outputXmlFile);
		System.out.println("output == > " + outputXmlFile);
	}







}
