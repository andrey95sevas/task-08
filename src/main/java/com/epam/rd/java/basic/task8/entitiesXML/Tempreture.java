package com.epam.rd.java.basic.task8.entitiesXML;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "value"
})
public class Tempreture {

    @XmlValue
    @XmlSchemaType(name = "positiveInteger")
    protected int tempValue;
    @XmlAttribute(name = "measure", required = true)
    protected String tempMeasure;

    public Tempreture() {
    }

    public Tempreture(Integer value, String measure) {
        tempValue = value;
        tempMeasure = measure;
    }

    public Integer getTempValue() {
        return tempValue;
    }

    public void setTempValue(int value) {
        tempValue = value;
    }

    public String getTempMeasure() {
        if (tempMeasure == null) {
            return "celcius";
        } else {
            return tempMeasure;
        }
    }

    public void setTempMeasure(String value) {
        tempMeasure = value;
    }

    @Override
    public String toString() {
        return "Tempreture{" +
                "tempValue=" + tempValue +
                ", tempMeasure='" + tempMeasure + '\'' +
                '}';
    }
}
