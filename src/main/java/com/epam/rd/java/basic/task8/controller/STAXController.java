package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entitiesXML.*;
import com.epam.rd.java.basic.task8.handler.FlowersXmlTag;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {
	String xmlFile;
	Flowers flowers;
	Flower flower;
	GrowingTips growingTips;
	VisualParameters visualParameters;
	AveLenFlower aveLenFlower;
	Tempreture tempreture;
	Lighting lighting;
	Watering watering;
	String currentElement;
	static final String MEASURE = "measure";

	public Flowers getFlowers() {
		return flowers;
	}

	public STAXController(String xmlFile) {
		this.xmlFile = xmlFile;
	}

	public void parse() throws XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		xmlInputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
		xmlInputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
		XMLEventReader reader = xmlInputFactory.createXMLEventReader(new StreamSource(xmlFile));
		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();
			if (event.isCharacters() && event.asCharacters().isWhiteSpace())
				continue;
			if (event.isStartElement()) {
				StartElement startElement = event.asStartElement();
				currentElement = startElement.getName().getLocalPart();
				if (FlowersXmlTag.FLOWERS.equalsTo(currentElement)) {
					flowers = new Flowers();
					continue;
				}
				if (FlowersXmlTag.FLOWER.equalsTo(currentElement)) {
					flower = new Flower();
					continue;
				}
				if (FlowersXmlTag.AVELENFLOWER.equalsTo(currentElement)) {
					aveLenFlower = new AveLenFlower();
					String flowerLength = startElement.getAttributeByName(new QName(MEASURE)).getValue();
					if (flowerLength != null) {
						aveLenFlower.setMeasure(flowerLength);
						continue;
					}
				}
				if (FlowersXmlTag.TEMPRETURE.equalsTo(currentElement)) {
					tempreture = new Tempreture();
					String tempMeasure = startElement.getAttributeByName(new QName(MEASURE)).getValue();
					if (tempMeasure != null) {
						tempreture.setTempMeasure(tempMeasure);
						continue;
					}
				}
				if (FlowersXmlTag.LIGHTING.equalsTo(currentElement)) {
					lighting = new Lighting();
					String light = startElement.getAttributeByName(new QName("lightRequiring")).getValue();
					if (light != null) {
						lighting.setLightRequiring(light);
						continue;
					}
				}
				if (FlowersXmlTag.WATERING.equalsTo(currentElement)) {
					watering = new Watering();
					String wateringMeasure = startElement.getAttributeByName(new QName(MEASURE)).getValue();
					if (wateringMeasure != null) {
						watering.setMeasure(wateringMeasure);
						continue;
					}
				}
			}
			if (event.isCharacters()) {
				Characters characters = event.asCharacters();
				if (FlowersXmlTag.NAME.equalsTo(currentElement)) {
					flower.setName(characters.getData());
					continue;
				}
				if (FlowersXmlTag.SOIL.equalsTo(currentElement)) {
					flower.setSoil(characters.getData());
					continue;
				}
				if (FlowersXmlTag.ORIGIN.equalsTo(currentElement)) {
					flower.setOrigin(characters.getData());
					continue;
				}
				if (FlowersXmlTag.STEMCOLOUR.equalsTo(currentElement)) {
					visualParameters = new VisualParameters();
					visualParameters.setStemColour(characters.getData());
					continue;
				}
				if (FlowersXmlTag.LEAFCOLOUR.equalsTo(currentElement)) {
					visualParameters.setLeafColour(characters.getData());
					continue;
				}
				if (FlowersXmlTag.AVELENFLOWER.equalsTo(currentElement)) {
					aveLenFlower.setValue(Integer.parseInt(characters.getData()));
					continue;
				}
				if (FlowersXmlTag.TEMPRETURE.equalsTo(currentElement)) {
					growingTips = new GrowingTips();
					tempreture.setTempValue(Integer.parseInt(characters.getData()));
					continue;
				}
				if (FlowersXmlTag.LIGHTING.equalsTo(currentElement)) {
					lighting.setLightRequiring(characters.getData());
					continue;
				}
				if (FlowersXmlTag.WATERING.equalsTo(currentElement)) {
					watering.setValue(Integer.parseInt(characters.getData()));
					continue;
				}
				if (FlowersXmlTag.MULTIPLYING.equalsTo(currentElement)) {
					flower.setMultiplying(characters.getData());
					continue;
				}
			}
			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				String localName = endElement.getName().getLocalPart();
				switch (localName) {
					case "visualParameters":
						flower.visualParameters = new VisualParameters(visualParameters.getStemColour(), visualParameters.getLeafColour(), aveLenFlower);
						continue;
					case "growingTips":
						flower.growingTips = new GrowingTips(tempreture, lighting, watering);
						continue;
					default:
						break;
				}
				if (FlowersXmlTag.FLOWER.equalsTo(localName)) {
					flowers.getFlowerList().add(flower);
				}
			}
		}
		reader.close();
	}

}