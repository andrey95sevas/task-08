package com.epam.rd.java.basic.task8.entitiesXML;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "tempreture",
        "lighting",
        "watering"
})
public class GrowingTips {

    @XmlElement(required = true)
    public Tempreture tempreture;
    @XmlElement(required = true)
    public Lighting lighting;
    @XmlElement(required = true)
    public Watering watering;

    public GrowingTips() {
    }

    public GrowingTips(Tempreture tempreture, Lighting lighting, Watering watering) {
        this.tempreture = tempreture;
        this.lighting = lighting;
        this.watering = watering;
    }

    public Tempreture getTempreture() {
        return tempreture;
    }

    public Watering getWatering() {
        return watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "tempreture=" + tempreture +
                ", lighting=" + lighting +
                ", watering=" + watering +
                '}';
    }
}

