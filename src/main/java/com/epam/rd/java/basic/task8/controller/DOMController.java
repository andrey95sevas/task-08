package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entitiesXML.*;
import com.epam.rd.java.basic.task8.handler.FlowersXmlTag;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	Flowers flowers;
	private static final String MEASURE = "measure";

	public Flowers getFlowers() {
		return flowers;
	}


	public void parse(String fileName) {
		flowers = new Flowers();
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
			DocumentBuilder db;
			Document document;
			db = dbf.newDocumentBuilder();
			document = db.parse(new File(fileName));
			NodeList flowerNodes = document.getElementsByTagName("flower");
			for (int i = 0; i < flowerNodes.getLength(); i++) {
				flowers.getFlowerList().add(getFlower(flowerNodes.item(i), document));
			}
		} catch (ParserConfigurationException e) {
			Logger.getAnonymousLogger().log(Level.SEVERE, "Cannot parse using DOM", e);
		} catch (SAXException | IOException e) {
			Logger.getAnonymousLogger().log(Level.SEVERE, " cannot find file", e);
		}
	}

	private static Flower getFlower(Node node, Document document) {
		Flower flower = new Flower();
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			Element element = (Element) node;
			flower.setName(getTagValue(FlowersXmlTag.NAME.value(), element));
			flower.setSoil(getTagValue(FlowersXmlTag.SOIL.value(), element));
			flower.setOrigin(getTagValue(FlowersXmlTag.ORIGIN.value(), element));
			Node nodeVisualParameters = document.getElementsByTagName(FlowersXmlTag.VISUAL_PARAMETERS.value()).item(0);
			Element visualElement = (Element) nodeVisualParameters;
			String stemColour = getTagValue(FlowersXmlTag.STEMCOLOUR.value(), visualElement);
			String leafColour = getTagValue(FlowersXmlTag.LEAFCOLOUR.value(), visualElement);
			AveLenFlower aveLenFlower = new AveLenFlower();
			Node nodeAveLenFlower = document.getElementsByTagName(FlowersXmlTag.AVELENFLOWER.value()).item(0);
			aveLenFlower.setMeasure(nodeAveLenFlower.getAttributes().getNamedItem(MEASURE).getTextContent());
			aveLenFlower.setValue(Integer.parseInt(getTagValue(FlowersXmlTag.AVELENFLOWER.value(), visualElement)));
			flower.setVisualParameters(new VisualParameters(stemColour, leafColour, aveLenFlower));
			Node nodeGrowingTips = document.getElementsByTagName(FlowersXmlTag.GROWING_TIPS.value()).item(0);
			Element elementGrowingTips = (Element) nodeGrowingTips;
			Tempreture tempreture = new Tempreture();
			Node tmp = document.getElementsByTagName(FlowersXmlTag.TEMPRETURE.value()).item(0);
			tempreture.setTempMeasure(tmp.getAttributes().getNamedItem(MEASURE).getTextContent());
			tempreture.setTempValue(Integer.parseInt(getTagValue(FlowersXmlTag.TEMPRETURE.value(), elementGrowingTips)));
			Lighting lighting = new Lighting();
			Node nodeLigthing = document.getElementsByTagName(FlowersXmlTag.LIGHTING.value()).item(0);
			lighting.setLightRequiring(nodeLigthing.getAttributes().getNamedItem("lightRequiring").getTextContent());
			Watering watering = new Watering();
			Node nodeWater = document.getElementsByTagName(FlowersXmlTag.WATERING.value()).item(0);
			watering.setMeasure(nodeWater.getAttributes().getNamedItem(MEASURE).getTextContent());
			watering.setValue(Integer.parseInt(getTagValue(FlowersXmlTag.WATERING.value(), elementGrowingTips)));
			flower.setGrowingTips(new GrowingTips(tempreture, lighting, watering));
			flower.setMultiplying(getTagValue(FlowersXmlTag.MULTIPLYING.value(), element));
		}
		return flower;
	}

	public static String getTagValue(String tag, Element element) {
		String result = null;
		NodeList list = element.getElementsByTagName(tag);
		if (list != null && list.getLength() > 0) {
			NodeList subList = list.item(0).getChildNodes();
			if (subList != null && subList.getLength() > 0) {
				result = subList.item(0).getNodeValue();
			}
		}
		return result;
	}

	public static Document getDocument(Flowers flowers) throws ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
		dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();

		Element element = document.createElement(FlowersXmlTag.FLOWERS.value());
		document.appendChild(element);

		for (Flower flower : flowers.getFlowerList()) {
			Element gElement = document.createElement(FlowersXmlTag.FLOWER.value());
			element.appendChild(gElement);

			Element nameElement = document.createElement(FlowersXmlTag.NAME.value());
			nameElement.setTextContent(flower.getName());
			gElement.appendChild(nameElement);

			Element soilElement = document.createElement(FlowersXmlTag.SOIL.value());
			soilElement.setTextContent(flower.getSoil());
			gElement.appendChild(soilElement);

			Element originElement = document.createElement(FlowersXmlTag.ORIGIN.value());
			originElement.setTextContent(flower.getOrigin());
			gElement.appendChild(originElement);

			Element visualParameters = document.createElement(FlowersXmlTag.VISUAL_PARAMETERS.value());
			gElement.appendChild(visualParameters);
			Element stemElement = document.createElement(FlowersXmlTag.STEMCOLOUR.value());
			stemElement.setTextContent(flower.getVisualParameters().getStemColour());
			visualParameters.appendChild(stemElement);
			Element leafElement = document.createElement(FlowersXmlTag.LEAFCOLOUR.value());
			leafElement.setTextContent(flower.getVisualParameters().getLeafColour());
			visualParameters.appendChild(leafElement);
			Element aveLenElement = document.createElement(FlowersXmlTag.AVELENFLOWER.value());
			aveLenElement.setTextContent(String.valueOf(flower.getVisualParameters().getAveLenFlower().getValue()));
			aveLenElement.setAttribute(MEASURE, flower.getVisualParameters().getAveLenFlower().measure);
			visualParameters.appendChild(aveLenElement);

			Element growingTips = document.createElement(FlowersXmlTag.GROWING_TIPS.value());
			gElement.appendChild(growingTips);
			Element tempElement = document.createElement(FlowersXmlTag.TEMPRETURE.value());
			tempElement.setTextContent(String.valueOf(flower.getGrowingTips().getTempreture().getTempValue()));
			tempElement.setAttribute(MEASURE, flower.getGrowingTips().getTempreture().getTempMeasure());
			growingTips.appendChild(tempElement);
			Element lightelement = document.createElement(FlowersXmlTag.LIGHTING.value());
			lightelement.setAttribute("lightRequiring", "yes");
			growingTips.appendChild(lightelement);
			Element waterElement = document.createElement(FlowersXmlTag.WATERING.value());
			waterElement.setTextContent(String.valueOf(flower.getGrowingTips().getWatering().getValue()));
			waterElement.setAttribute(MEASURE, flower.getGrowingTips().getWatering().getMeasure());
			growingTips.appendChild(waterElement);

			Element multiplyingElement = document.createElement(FlowersXmlTag.MULTIPLYING.value());
			multiplyingElement.setTextContent(flower.getMultiplying());
			gElement.appendChild(multiplyingElement);


		}
		return document;
	}

	public static void saveToXML(Flowers flowers, String xmlFileName) throws ParserConfigurationException, TransformerException {
		saveToXML(getDocument(flowers), xmlFileName);
	}

	public static void saveToXML(Document document, String xmlFileName) throws TransformerException {
		StreamResult result = new StreamResult(new File(xmlFileName));

		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setParameter(XMLConstants.FEATURE_SECURE_PROCESSING, true);
		transformer.setParameter(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		transformer.setParameter(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		transformer.transform(new DOMSource(document), result);
	}

}
