package com.epam.rd.java.basic.task8.entitiesXML;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "stemColour",
        "leafColour",
        "aveLenFlower"
})
public class VisualParameters {

    @XmlElement(required = true)
    public String stemColour;
    @XmlElement(required = true)
    public String leafColour;
    protected AveLenFlower aveLenFlower;

    public VisualParameters() {
    }

    public VisualParameters(String stemColour, String leafColour, AveLenFlower aveLenFlower) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String value) {
        this.stemColour = value;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String value) {
        this.leafColour = value;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    @Override
    public String toString() {
        return "VisualParameters:" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower + "";
    }
}
