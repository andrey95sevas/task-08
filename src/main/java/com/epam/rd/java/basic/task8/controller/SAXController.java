package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entitiesXML.*;
import com.epam.rd.java.basic.task8.handler.Constants;
import com.epam.rd.java.basic.task8.handler.FlowersXmlTag;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
    private Flowers flowers;
    String name;
    String soil;
    String origin;
    String stemColour;
    String leafColour;
    String aveLenFlowerMeasure;
    int aveLenFlowerValue;
    String tempMeasure;
    int tempValue;
    String lightingRequiring;
    String waterMeasure;
    int waterValue;
    String multiplying;
    String current;
    private static final String MEASURE = "measure";
    private String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Flowers getFlowers() {
        return flowers;
    }

    public void parse(boolean validate) throws SAXException, ParserConfigurationException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        if (validate) {
            factory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
            factory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
        }
        SAXParser parser = factory.newSAXParser();
        parser.parse(xmlFileName, this);
    }

    @Override
    public void startDocument() {
        flowers = new Flowers();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attrs) {
        current = localName;

        if (FlowersXmlTag.AVELENFLOWER.equalsTo(current)) {
            aveLenFlowerMeasure = attrs.getValue(MEASURE);
            return;
        }
        if (FlowersXmlTag.TEMPRETURE.equalsTo(current)) {
            tempMeasure = attrs.getValue(MEASURE);
            return;
        }
        if (FlowersXmlTag.LIGHTING.equalsTo(current)) {
            lightingRequiring = attrs.getValue("lightRequiring");
            return;
        }
        if (FlowersXmlTag.WATERING.equalsTo(current)) {
            waterMeasure = attrs.getValue(MEASURE);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String elementText = new String(ch, start, length).trim();
        if (elementText.isEmpty()) return;

        if (FlowersXmlTag.NAME.equalsTo(current)) {
            name = elementText;
            return;
        }
        if (FlowersXmlTag.SOIL.equalsTo(current)) {
            soil = elementText;
            return;
        }
        if (FlowersXmlTag.ORIGIN.equalsTo(current)) {
            origin = elementText;
            return;
        }
        if (FlowersXmlTag.STEMCOLOUR.equalsTo(current)) {
            stemColour = elementText;
            return;
        }
        if (FlowersXmlTag.LEAFCOLOUR.equalsTo(current)) {
            leafColour = elementText;
            return;
        }
        if (FlowersXmlTag.AVELENFLOWER.equalsTo(current)) {
            aveLenFlowerValue = Integer.parseInt(elementText);
            return;
        }
        if (FlowersXmlTag.TEMPRETURE.equalsTo(current)) {
            tempValue = Integer.parseInt(elementText);
            return;
        }
        if (FlowersXmlTag.WATERING.equalsTo(current)) {
            waterValue = Integer.parseInt(elementText);
            return;
        }
        if (FlowersXmlTag.MULTIPLYING.equalsTo(current)) {
            multiplying = elementText;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (FlowersXmlTag.FLOWER.equalsTo(qName)) {
            flowers.getFlowerList().add(new Flower(name, soil, origin,
                    new VisualParameters(stemColour, leafColour, new AveLenFlower(aveLenFlowerValue, aveLenFlowerMeasure)),
                    new GrowingTips(new Tempreture(tempValue, tempMeasure), new Lighting(lightingRequiring), new Watering(waterValue, waterMeasure)), multiplying));
        }
    }
}