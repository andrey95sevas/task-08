package com.epam.rd.java.basic.task8.handler;

public enum FlowersXmlTag {
    FLOWERS("flowers"), FLOWER("flower"), NAME("name"), SOIL("soil"),
    ORIGIN("origin"), VISUAL_PARAMETERS("visualParameters"), STEMCOLOUR("stemColour"), LEAFCOLOUR("leafColour"),
    AVELENFLOWER("aveLenFlower"), GROWING_TIPS("growingTips"), TEMPRETURE("tempreture"),
    LIGHTING("lighting"), WATERING("watering"), MULTIPLYING("multiplying");

    private final String value;

    FlowersXmlTag(String value) {
        this.value = value;
    }

    public boolean equalsTo(String name) {
        return value.equals(name);
    }

    public String value() {
        return value;
    }
}
