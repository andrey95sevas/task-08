package com.epam.rd.java.basic.task8.handler;

public final class Constants {
    public static final String OUTPUT = "output == > ";
    public static final String FEATURE_TURN_VALIDATION_ON = "http://xml.org/sax/features/validation";
    public static final String FEATURE_TURN_SCHEMA_VALIDATION_ON = "http://apache.org/xml/features/validation/schema";


    Constants() {
    }
}
