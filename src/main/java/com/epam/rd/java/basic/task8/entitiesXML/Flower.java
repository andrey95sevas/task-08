package com.epam.rd.java.basic.task8.entitiesXML;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "name",
        "soil",
        "origin",
        "visualParameters",
        "growingTips",
        "multiplying"
})
public class Flower {

    @XmlElement(required = true)
    public String name;
    @XmlElement(required = true)
    public String soil;
    @XmlElement(required = true)
    public String origin;
    @XmlElement(required = true)
    public VisualParameters visualParameters;
    @XmlElement(required = true)
    public GrowingTips growingTips;
    @XmlElement(required = true)
    public String multiplying;

    public Flower() {
    }

    public Flower(String name, String soil, String origin, VisualParameters visualParameters, GrowingTips growingTips, String multiplying) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.visualParameters = visualParameters;
        this.growingTips = growingTips;
        this.multiplying = multiplying;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String value) {
        this.soil = value;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String value) {
        this.origin = value;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters value) {
        this.visualParameters = value;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips value) {
        this.growingTips = value;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String value) {
        this.multiplying = value;
    }

    @Override
    public String toString() {
        return "FlowerPojo [growingTips = " + growingTips + ", multiplying = " + multiplying + ", origin = " + origin + ", name = " + name + ", visualParameters = " + visualParameters + ", soil = " + soil + "]";
    }
}
