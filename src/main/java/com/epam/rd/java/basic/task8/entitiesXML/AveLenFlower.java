package com.epam.rd.java.basic.task8.entitiesXML;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "value"
})
public class AveLenFlower {

    @XmlValue
    @XmlSchemaType(name = "positiveInteger")
    public Integer value;
    @XmlAttribute(name = "measure", required = true)
    public String measure;

    public AveLenFlower() {
    }

    public AveLenFlower(Integer value, String measure) {
        this.value = value;
        this.measure = measure;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public void setMeasure(String value) {
        this.measure = value;
    }

    @Override
    public String toString() {
        return "AveLenFlower{" +
                "value=" + value +
                ", measure='" + measure + '\'' +
                '}';
    }
}



