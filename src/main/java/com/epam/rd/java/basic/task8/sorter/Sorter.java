package com.epam.rd.java.basic.task8.sorter;

import com.epam.rd.java.basic.task8.entitiesXML.Flower;
import com.epam.rd.java.basic.task8.entitiesXML.Flowers;

import java.util.Comparator;

public class Sorter {
    Sorter() {
    }

    private static final Comparator<Flower> sortFlowersByName = Comparator.comparing(Flower::getName);

    public static void sortByName(Flowers flowers) {
        flowers.getFlowerList().sort(sortFlowersByName);
    }

    private static final Comparator<Flower>
            sortFLowerBySoil = Comparator.comparing(Flower::getSoil);

    public static void sortBySoil(Flowers flowers) {
        flowers.getFlowerList().sort(sortFLowerBySoil);
    }

    private static final Comparator<Flower>
            sortFLowerByOrigin = Comparator.comparing(Flower::getOrigin);

    public static void sortByOrigin(Flowers flowers) {
        flowers.getFlowerList().sort(sortFLowerByOrigin);
    }
}

